document.addEventListener("DOMContentLoaded", setup);

function setup() {
    document.querySelector('#logForm').addEventListener('submit',checkLogin);
}

function checkLogin(e)
{
    e.preventDefault()
    let username = document.querySelector("#username").value;
    let password = document.querySelector("#password").value;
    let url = "http://127.0.0.1:5000/api/login?username="+username+"&password="+password;

    //Makes a request to the url
    fetch(url)
        .then(
            /**When it gets its response it goes through an anonymous function that decides what to do with the response
             *@param response reponse of the request made by fetch
             */
            function (response) {
                //Asks if the response was a good one.
                if (response.ok) {
                    //gives back the answer in json format.
                    return response.json();
                }
                else {
                    //Throw and error and its code if the response was anything else than successful
                    throw new Error('Status code: ' + response.status + '. Please try again.')
                }
            }
        )
        .then(answer => login(answer))
        //If there is an error it catches it and gives it to the treatError function
        .catch(error => treatError(error));
}

function login(answer)
{
    if (answer != null)
    {
        location.href = 'profil/index.html';
    }
    else
    {
        treatError("Wrong Username or Password.");
    }
}

function treatError(message)
{
    let containor = document.querySelector("#errorMsg")
    containor.textContent = message;
}
