# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 11:59:52 2021

@author: Liliane, Le
"""

import mysql.connector
from mysql.connector import errorcode
from DB.DBConn import cursor
from DB.DBConn import mydb

DB_NAME = "marihacks"

TABLES = {}

TABLES['users'] = (
        "CREATE TABLE users (\
        user_id INT AUTO_INCREMENT PRIMARY KEY,\
        username VARCHAR(255),\
        first_name VARCHAR(255),\
        last_name VARCHAR(255),\
        email VARCHAR(255),\
        role VARCHAR(255),\
        hashedPassword VARCHAR(255)\
        )"
    )
    

TABLES['careers'] = (
        "CREATE TABLE careers (\
        career_id INT AUTO_INCREMENT PRIMARY KEY,\
        career_name VARCHAR(255)\
        )"
    )

TABLES['interests'] = (
        "CREATE TABLE interests (\
        interest_id INT AUTO_INCREMENT PRIMARY KEY,\
        interest_name VARCHAR(255)\
        )"
    )

TABLES['user_interests'] = (
        "CREATE TABLE user_interests (\
        user_id INT,\
        interest_id INT,\
        CONSTRAINT fk_user_id \
        FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,\
        CONSTRAINT fk_interest_id\
        FOREIGN KEY (interest_id) REFERENCES interests(interest_id) ON DELETE CASCADE\
        )"
    )
    
TABLES['user_careers'] = (
        "CREATE TABLE user_careers (\
        user_id INT,\
        career_id INT,\
        CONSTRAINT fk_user_id_career_id \
        FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,\
        CONSTRAINT fk_career_id\
        FOREIGN KEY (career_id) REFERENCES careers(career_id) ON DELETE CASCADE\
        )"
    )
 
TABLES['mentor_student'] = (
        "CREATE TABLE mentor_student(\
        mentor_id INT,\
        student_id INT\
        )"
    )

def create_database():
    """
    Function that creates the database
    
    Returns
    -------
    None.

    """
    cursor.execute("CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'".format(DB_NAME))
    print("DATABASE {} CREATED".format(DB_NAME))

def create_tables():
    """
    Function that creates every table in the TABLES dictionary

    Returns
    -------
    None.

    """
    cursor.execute("USE {}".format(DB_NAME))
    cursor.execute("DROP TABLE IF EXISTS {}".format('user_interests'))
    cursor.execute("DROP TABLE IF EXISTS {}".format('user_careers'))
    cursor.execute("DROP TABLE IF EXISTS {}".format('users'))
    cursor.execute("DROP TABLE IF EXISTS {}".format('careers'))
    cursor.execute("DROP TABLE IF EXISTS {}".format('interests'))
    cursor.execute("DROP TABLE IF EXISTS {}".format('mentor_student'))
    
    cursor.execute(TABLES['users'])
    print("Inserted table: {}".format('users'))
    
    
    cursor.execute(TABLES['careers'])
    print("Inserted table: {}".format('careers'))
    
    
    cursor.execute(TABLES['interests'])
    print("Inserted table: {}".format('interests'))
    
    cursor.execute(TABLES['user_interests'])
    print("Inserted table: {}".format('user_interests'))
    
    cursor.execute(TABLES['user_careers'])
    print("Inserted table: {}".format('user_careers'))
    
    cursor.execute(TABLES['mentor_student'])
    print("Inserted table: {}".format('mentor_student'))
    
    # for table in TABLES:
    #     try:
    #         cursor.execute("DROP TABLE IF EXISTS {}".format(table))
    #         cursor.execute(TABLES[table])
    #         print("Inserted table: {}".format(table))
    #     except mysql.connector.Error as err:
    #         if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
    #             print("Already Exists")
    #         else :
    #             print(err.msg)

def readFromFile(filename):
    """
    Reads from file and returns all the lines 

    Parameters
    ----------
    filename : String
        path to file

    Returns
    -------
    lines : list
        list that contains all lines

    """
    file = open(filename,"r")
    lines = file.readlines()
    lines = [line.replace("\n","") for line in lines]
    return lines

def insert_career_list():
    """
    Function that inserts careers from a file with all careers

    Returns
    -------
    None.

    """
    cursor.execute("USE {}".format(DB_NAME))
    careers = readFromFile("DB\data\career_goal_comma.txt")
    career_split = [career.split(",")[:1] for career in careers]
    
    sql = "INSERT INTO careers (career_name) VALUES(%s)"
    cursor.executemany(sql,career_split)
    mydb.commit()

def insert_interest():
    """
    Function that inserts into interests from a file with most interests

    Returns
    -------
    None.

    """
    cursor.execute("USE {}".format(DB_NAME))
    interests = readFromFile("DB\data\interest.txt")
    interests_split = [interest.split(",")[:1] for interest in interests]
    sql = "INSERT INTO interests (interest_name) VALUES(%s)"
    cursor.executemany(sql,interests_split)
    mydb.commit()
