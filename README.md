#Mentor Connector 
Mentor Connector is a project for the MariHacks 2021 event. It lets the user creates an account as student or mentor. It then allow them to login, change their informations if needed and show them the possible matches with a mentor.

##Instructions
1. In "backend/DB/DBConn.py", change the host to informations to correspond to your personnal MySql database informations
2. In MySql WorkBench, create the schema marihacks
3. In "backend/main.py", uncomment line 10-13 and run it
4. Run api.py

\* Make sure your python has flask
** Unfortunately, this project has not been completed.
