import json
from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("content-type","text/html")
        self.end_headers()
        self.wfile.write("hello world!".encode())

def main():
    PORT=8000
    server = HTTPServer(("",PORT), SimpleHTTPRequestHandler)
    print("Server running on port %s" % PORT)
    server.serve_forever()

if (__name__ == "__main__"):
    main()


