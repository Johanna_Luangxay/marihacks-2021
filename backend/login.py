# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 11:59:52 2021

@author: Liliane, Le
"""
import mysql.connector
from users import users
from mysql.connector import errorcode
from DB.DBConn import cursor
from DB.DBConn import mydb

DB_NAME = "marihacks"
cursor.execute("USE {}".format(DB_NAME))

user = users()
def create_new_user(**user_info):
    users.create_new_user(**user_info)

def login_user(username,password):
    """
    Functions that authenticates username and password.
    Checks if the exist in the database

    Parameters
    ----------
    username : String
        Username of the user
    password : String
        Password of the user

    Returns
    -------
    boolean
        Represents if a user successfully loggind in or not

    """
    
    sql = "SELECT * FROM users WHERE username = \"{0}\" AND hashedPassword = SHA1(\"{1}\")".format(username,password) 
    
    cursor.execute(sql)
    result = cursor.fetchall()
    
    if result:
        print("Login Sucessful")
        return users.getDetailedUserInfo(username)
    else :
        print("Login Failed")
        return None
