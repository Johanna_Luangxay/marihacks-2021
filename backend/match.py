# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 16:03:59 2021

@author: leduy
"""

import mysql.connector
import json
from mysql.connector import errorcode
from DB.DBConn import cursor
from DB.DBConn import mydb
DB_NAME = "marihacks"
def best_match_careers(username,career_list,role):
    """
    Finds the best match for careers

    Parameters
    ----------
    username : String
        Username of the user 
    career_list : list
        containing all careers of a user
    role : String
        the rule of the user(student/mentor)

    Returns
    -------
    jsonstr : String
        Every user that matches with every career in the list

    """
    cursor.execute("USE {}".format(DB_NAME))
    matches = []
    for career in career_list:
        sql = "SELECT first_name FROM users JOIN user_careers USING(user_id)JOIN careers USING(career_id)WHERE career_name = \"{0}\" AND username != \"{1}\" AND role != \"{2}\"".format(career,username,role) 
        cursor.execute(sql)
        result = cursor.fetchone()
        matches.append(result)
    jsonstr = json.dumps(matches)
    print(jsonstr)
    return jsonstr

def best_match_interests(username, interest_list,role):
    """
    Finds the best match for interests

    Parameters
    ----------
    username : String
        Username of the user
    interest_list : List
        containing all careers of a user
    role : String
        the role of the user (student/mentor)
        
    Returns
    -------
    jsonstr : TYPE
        DESCRIPTION.

    """
    cursor.execute("USE {}".format(DB_NAME))
    matches = []
    for interest in interest_list:
        sql = "SELECT first_name FROM users JOIN user_interests USING(user_id)JOIN interests USING(interest_id)WHERE interest_name = \"{0}\" AND username != \"{1}\" AND role != \"{2}\"".format(interest,username,role ) 
        cursor.execute(sql)
        result = cursor.fetchone()
        matches.append(result)
    jsonstr = json.dumps(matches)
    print(jsonstr)
    return jsonstr 

best_match_careers("bb",["Accountant","Agricultural Engineer"],'student')
best_match_interests("bb",["Candle making","Dance"],'student')