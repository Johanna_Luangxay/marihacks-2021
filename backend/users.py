# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 16:50:18 2021

@author: leduy
"""
import mysql.connector
from DB.DBConn import cursor
from DB.DBConn import mydb
DB_NAME = "marihacks"
cursor.execute("USE {}".format(DB_NAME))
class users :
    
    def create_new_user(**user_info):
        """
        Adds the user to the database 
    
        Parameters
        ----------
        **user_info : **kwargs
            All information for a new user giving in JSON
    
        Returns
        -------
        None.
    
        """
        try:
            sql = "INSERT INTO users (username, first_name, last_name, email, role, hashedPassword) VALUES (%s, %s, %s, %s, %s, SHA1(%s))"
            val = (user_info.get("username"),
                   user_info.get("first_name"),
                   user_info.get("last_name"),
                   user_info.get("email"),
                   user_info.get("role"),
                   user_info.get("hashedPassword"))
            cursor.execute(sql,val)
            mydb.commit()
            print("user_added")
            print(val)
        except mysql.connector.Error:
            print("Error: User not inserted")
            
    
    def addCareers(username,career_list):
        """
        Method that adds list of careers to a user in the database

        Parameters
        ----------
        username : String
            Username of the user
        career_list : List
            List containing all the careers

        Returns
        -------
        None.

        """
        sql = "SELECT user_id FROM users WHERE username = \"{}\"".format(username)
        cursor.execute(sql)
        user_id = str(cursor.fetchone()[0])
        for career in career_list:
            sql = "SELECT career_id FROM careers WHERE career_name = \"{}\"".format(career)
            cursor.execute(sql)
            career_id = str(cursor.fetchone()[0])
            
            insert = "INSERT INTO user_careers (user_id,career_id) VALUES(%s,%s)"
            val = [user_id,career_id]
            cursor.execute(insert,val)
            mydb.commit()
    
    def addInterest(username,interest_list):
        """
         Method that adds list of interests to a user in the database

        Parameters
        ----------
        username : String
            User name of the user
        interest_list : List
            List containing all the interests

        Returns
        -------
        None.

        """
        sql = "SELECT user_id FROM users WHERE username = \"{}\"".format(username)
        cursor.execute(sql)
        user_id = str(cursor.fetchone()[0])
        
        for interest in interest_list:
            sql = "SELECT interest_id FROM interests WHERE interest_name = \"{}\"".format(interest)
            cursor.execute(sql)
            interest_id = str(cursor.fetchone()[0])
            
            insert = "INSERT INTO user_interests (user_id,interest_id) VALUES(%s,%s)"
            val = [user_id,interest_id]
            cursor.execute(insert,val)
            mydb.commit()
    
    def getUser(username):
        """
        Methods that gets a user's information

        Parameters
        ----------
        username : String
            Username of the user

        Returns
        -------
        result : String
            info of the user

        """
        cursor = mydb.cursor(dictionary=True)
        sql = "SELECT first_name,last_name,email FROM users WHERE username = \"{}\"".format(username)
        cursor.execute(sql)
        result = cursor.fetchone()
        return result
    
    def getUserCareers(username):
        """
        Methods that gets all the users's careers'

        Parameters
        ----------
        username : Strin
            Username of the user

        Returns
        -------
        jsonstr : List
            All the careers in json

        """
        sql = "SELECT career_name FROM careers JOIN user_careers USING(career_id) JOIN users USING(user_id) WHERE username =  \"{}\"".format(username)
        cursor.execute(sql)
        myresult = cursor.fetchall()
        result = []
        """
        Each value inslide myresult is a tuple.
        Since it is only a single value inside those tuple, this loop gets those values and
        insert it into result list.
        """
        for x in myresult:
            result.append(x[0])
        
        return result
    
    def getUserInterests(username):
        """
        Methods that gets all user's interest'

        Parameters
        ----------
        username : Strin
            Username of the user
            
        Returns
        -------
        result : List
            All the interests 
        """
        sql = "SELECT interest_name FROM interests JOIN user_interests USING(interest_id) JOIN users USING(user_id) WHERE username =  \"{}\"".format(username)
        cursor.execute(sql)
        myresult = cursor.fetchall()
        result = []
        """
        Each value inslide myresult is a tuple.
        Since it is only a single value inside those tuple, this loop gets those values and
        insert it into result list.
        """
        for x in myresult:
            result.append(x[0])
        
        return result
    
    def getDetailedUserInfo(username): 
        """
        Combies the user, interest, and careers dictionaries

        Parameters
        ----------
        username : String
            Username of the user

        Returns
        -------
        user : dict
            contains all user information

        """
        career_name = ["careers"]
        career_dict = {}
        career_dict = career_dict.fromkeys(career_name,users.getUserCareers(username))
        user = users.getUser(username)
        """
        User might not exist in the database. 
        It will return an empty dict if that is the case.
        Otherwise, return user's info
        """
        if user is not None:
            
            user.update(career_dict)
            
            career_name = ["interest"]
            interest_dict = {}
            interest_dict = career_dict.fromkeys(career_name,users.getUserInterests(username))
            user.update(interest_dict)
            return user
        else:
            return {}
        
    def getAllUsers():
        """
        Combies all users including the user' info, interest, and careers dictionaries

        Returns
        -------
        allusers : list
            contains all user information

        """
        sql = "SELECT username FROM users"
        cursor.execute(sql)
        myresult = cursor.fetchall()
        result = []
        for x in myresult:
            result.append(x[0])
        
        allusers = []
        for username in result:
            allusers.append(users.getDetailedUserInfo(username))
        
        return allusers