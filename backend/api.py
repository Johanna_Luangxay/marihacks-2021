# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 23:01:45 2021

@author: Vashti
"""

import flask
from flask_cors import CORS, cross_origin
from flask import request, jsonify
from DB.DBConn import cursor
from users import users 
import login


app = flask.Flask(__name__)
app.config["DEBUG"] = True


# cursor.execute('SELECT username, first_name,last_name,email,role,hashedPassword, interests.interest_name,careers.career_name FROM users JOIN user_interests USING (user_id) JOIN interests USING (interest_id) JOIN user_careers USING (user_id) JOIN careers USING (career_id)')
# myresult = cursor.fetchall()
# users = []

# for x in myresult:
#     users.append({'username': x[0], 'first_name': x[1], 'last_name': x[2],'email': x[3],'role': x[4],'interest_name': x[5],'career_name': x[6]})
# print(users)

# Create some test data for our catalog in the form of a list of dictionaries.
books = [
    {'id': 0,
     'title': 'A Fire Upon the Deep',
     'author': 'Vernor Vinge',
     'first_sentence': 'The coldsleep itself was dreamless.',
     'year_published': '1992'},
    {'id': 1,
     'title': 'The Ones Who Walk Away From Omelas',
     'author': 'Ursula K. Le Guin',
     'first_sentence': 'With a clamor of bells that set the swallows soaring, the Festival of Summer came to the city Omelas, bright-towered by the sea.',
     'published': '1973'},
    {'id': 2,
     'title': 'Dhalgren',
     'author': 'Samuel R. Delany',
     'first_sentence': 'to wound the autumnal city.',
     'published': '1975'}
]

@app.route('/', methods=['GET'])
def home():
    return '''<h1>Distant Reading Archive</h1>
<p>A prototype API for distant reading of science fiction novels.</p>'''


@app.route('/api/users/all', methods=['GET'])
def api_all():
    response = flask.jsonify(users.getAllUsers())
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/users', methods=['GET'])
def api_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser.
    if 'username' in request.args:
        username = str(request.args['username'])
    else:
        return jsonify({'status': 'error'})
    
    print(request.args)
    # Create an empty list for our results
    #results = []

    # Loop through the data and match results that fit the requested ID.
    # IDs are unique, but other fields might return many results
    # for user in users:
    #     if user['username'] == username:
    #         results.append(user)
    
    #I used the backend methods from users to get info from the database
    #It returns the  something close to good. It is an issue with having tuples inside a dict inside a list

    result = users.getDetailedUserInfo(username)
    # Use the jsonify function from Flask to convert our list of
    # Python dictionaries to the JSON format.
    
    if not bool(result):
        response = flask.jsonify({'status': 'error'});
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    response = flask.jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')    
    return response

@app.route('/api/login', methods=['GET'])
def api_login():
    # Check if an username and password was provided as part of the URL.
    # If username and is provided, assign it to a variable.
    # If not is provided, display an error in the browser.
    if 'username' in request.args and 'password' in request.args:
        username = str(request.args['username'])
        password = str(request.args['password'])
    else:
        response = flask.jsonify({'status': 'error'})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

    result = login.login_user(username,password)
    if result is None:
        response = flask.jsonify({'status': 'error'})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    # Use the jsonify function from Flask to convert our list of
    # Python dictionaries to the JSON format.
    response = flask.jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route(('/api/register'), methods=['GET'])
def api_register():
    if 'email' in request.args and 'role' in request.args and 'first_name' in request.args and 'last_name' in request.args and 'username' in request.args and 'password' in request.args and 'careers' in request.args and 'interest' in request.args:
        vusername = str(request.args['username'])
        vpassword = str(request.args['password'])
        vfirst_name = str(request.args['first_name'])
        vlast_name = str(request.args['last_name'])
        vcareers = str(request.args['careers'])
        vinterest = str(request.args['interest'])
        vrole = str(request.args['role'])
        vemail = str(request.args['email'])
    else:
        return jsonify({'status': 'error'})
    
    users.create_new_user(username = vusername, password = vusername,first_name = vfirst_name,last_name = vlast_name,email = vemail,role = vrole)
    users.addCareers(vusername,vcareers.split(','))
    users.addInterest(vusername,vinterest.split(','))
    response = flask.jsonify({'status': 'good'})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

app.run()